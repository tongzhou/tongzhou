#!/bin/bash - 
#===============================================================================
#
#          FILE: dhcp_configuration.sh
# 
#         USAGE: ./dhcp_configuration.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 19/04/16 15:24
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

yum -y install dhcp &> /dev/null
echo "********************************"
echo "***setting dhcp configuration***"
echo "********************************"

cat > /etc/dhcp/dhcpd.conf <<EOF
subnet 10.16.${RouterIP}.0 netmask 255.255.255.128 {
      option routers 10.16.${RouterIP}.126;
      option  domain-name-servers 10.16.${RouterIP}.126;
      range 10.16.14.${DHCPstart} 10.16.14.${DHCPend};
      host mail{
      option  domain-name-servers 10.16.${RouterIP}.126;
      hardware ethernet ${MAC};
      fixed-address 10.16.${RouterIP}.1;
      }
}
EOF
systemctl start dhcpd
systemctl enable dhcpd
systemctl restart dhcpd
echo "DHCP setting is done"

