yum -y install expect &> /dev/null
iptables -t filter -A INPUT  -p tcp --dport 25 -j ACCEPT
iptables -t filter -A INPUT  -p udp --dport 25 -j ACCEPT
iptables -t filter -A FORWARD  -p tcp --dport 25 -j ACCEPT
iptables -t filter -A FORWARD  -p udp --dport 25 -j ACCEPT
iptables -t filter -A INPUT  -p tcp --dport 110 -j ACCEPT
iptables -t filter -A INPUT  -p udp --dport 110 -j ACCEPT
iptables -t filter -A FORWARD  -p tcp --dport 110 -j ACCEPT
iptables -t filter -A FORWARD  -p udp --dport 110 -j ACCEPT
iptables -t filter -A INPUT  -p tcp --dport 143 -j ACCEPT
iptables -t filter -A INPUT  -p udp --dport 143 -j ACCEPT
iptables -t filter -A FORWARD  -p tcp --dport 143 -j ACCEPT
iptables -t filter -A FORWARD  -p udp --dport 143 -j ACCEPT
service iptables save
cat >> /etc/unbound/unbound.conf << EOF
forward-zone:
        name:"as2016.learn"
        forward-addr:142.232.221.253
EOF
systemctl restart unbound
expect ./ssh_to_mail.sh ${PASSWORD} 10.16.${RouterIP}.${MailIP} ${USERNAME} ${Mailname}

