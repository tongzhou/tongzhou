#!/bin/bash - 
#===============================================================================
#
#          FILE: dns_configuration.sh
# 
#         USAGE: ./dns_configuration.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 20/04/16 10:53
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

echo "*******************************"
echo "***setting dns configuration***"
echo "*******************************"
yum -y install nsd unbound &> /dev/null
systemctl enable nsd
systemctl start nsd
systemctl enable unbound
systemctl start unbound

cat > /etc/nsd/nsd.conf <<EOF
server:
      ip-address: 10.16.255.${RouterIP}
      do-ip6: no
      include: "/etc/nsd/server.d/*.conf"
      include: "/etc/nsd/conf.d/*.conf"
                          
remote-control:
      control-enable: yes
      zone:
      name: "${DOMname}"
      zonefile: "${DOMname}.zone"
      zone:
      name: "${RouterIP}.16.10.in-addr.arpa"
      zonefile: "${RouterIP}.16.10.in-addr.arpa.zone" 
EOF

cat > /etc/nsd/${DOMname}.zone <<EOF
;zone file for ${DOMname}
\$TTL 10s                                ; 10 secs default TTL for zone
${DOMname}.       IN SOA      ${Routername}. tongzhol0521.gmail.com.(
                        2016042001      ; serial number of Zone Record
                        1200s           ; refresh time
                        180s            ; update retry time on failure
                        1d              ; expiration time
                        3600            ; cache time to live
                        )
;Name servers for this domain
${DOMname}.       IN      NS      ${Routername}.
                                    MX   10 ${Mailname}.
;addresses of hosts
${Mailname}.  IN      A       10.16.${RouterIP}.${MailIP}

EOF
    
cat > /etc/nsd/${RouterIP}.16.10.in-addr.arpa.zone <<EOF
;zone file for 10.16.${RouterIP}.0 / ${Routername} reverse lookup
\$TTL 10s                                ;10 secs default TTL for zone
${RouterIP}.16.10.in-addr.arpa.   IN SOA         ${Routername}. tongzhol0521.gmail.com.(
                        2016042001      ; serial number of Zone Record
                        1200s           ; refresh time
                        180s            ; retry time on failure
                        1d              ; expiration time
                        3600            ; cache time to live
                        )
;Name servers for this domain
${RouterIP}.16.10.in-addr.arpa.          IN      NS      ${Routername}.
;IP to Hostname Pointers
${MailIP}.${RouterIP}.16.10.in-addr.arpa.        IN      PTR     ${Mailname}.
EOF

wget -S -N https://www.internic.net/domain/named.cache -O /etc/unbound/root.hints &> /dev/null
cat > /etc/unbound/unbound.conf << EOF
	server:
        interface:10.16.${RouterIP}.126
        interface:127.0.0.1
        port: 53
        do-ip4: yes
        do-ip6: no
         access-control: 0.0.0.0/0 refuse
         access-control: 127.0.0.0/8 allow
         access-control: ::0/0 refuse
         access-control: 10.16.${RouterIP}.0/25 allow
        chroot: ""
        username: "unbound"
        directory: "/etc/unbound"
        root-hints: "/etc/unbound/root.hints"
        pidfile: "/var/run/unbound/unbound.pid"
        private-domain:"ne.learn"
        private-domain:"htpbcit.ca"
        local-zone: "10.in-addr.arpa." nodefault
        local-zone: "16.172.in-addr.arpa." nodefault
        local-zone: "168.192.in-addr.arpa." nodefault
        prefetch: yes
        module-config: "iterator"
remote-control:
        control-enable: yes
        server-key-file: "/etc/unbound/unbound_server.key"
        server-cert-file: "/etc/unbound/unbound_server.pem"
        control-key-file: "/etc/unbound/unbound_control.key"
        control-cert-file: "/etc/unbound/unbound_control.pem"
include: /etc/unbound/conf.d/*.conf
stub-zone:
        name: "learn"
        stub-addr: 142.232.221.253
stub-zone:
        name: "htpbcit.ca"
        stub-addr: 142.232.221.253
stub-zone:
        name: "${RouterIP}.16.10.in-addr.arpa"
        stub-addr: 10.16.255.${RouterIP}
stub-zone:
        name: "255.16.10.in-addr.arpa"
        stub-addr: 142.232.221.253
stub-zone:
        name: "${Routername}"
        stub-addr: 10.16.255.${RouterIP}
forward-zone:
        name:"bcit.ca"
        forward-addr:142.232.221.253
EOF

systemctl restart nsd
systemctl restart unbound
systemctl restart network 
echo "DNS setting is done"

