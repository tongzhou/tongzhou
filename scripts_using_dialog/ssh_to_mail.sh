set password [lrange $argv 0 0]
set ipaddr [lrange $argv 1 1]
set name [lrange $argv 2 2]
set Mailname [lrange $argv 3 3]
spawn sftp $name@$ipaddr
expect "(yes/no)?"
send "yes\r"
expect "password: "
send "$password\r"
expect "sftp>"
send "put mail_configuration.sh\r"
send "put netdev_bootstrap.sh\r"
send "exit\r"
expect eof
spawn ssh $name@$ipaddr
expect "password: "
send "$password\r"
expect "~]#"
send "hostnamectl set-hostname ${Mailname}\r"
send "bash mail_configuration.sh\r"
interact

