echo "********************************"
echo "***preparing for installation***"
echo "********************************"
yum -y install dialog &> /dev/null
yesno() {
dialog --title "Welcome to Installation" \
       --backtitle "Installation" \
       --clear \
       --yesno "Press [OK] to continue" 16 51
result=$?
if [ $result -eq 1 ] ; then
exit 1;
elif [ $result -eq 255 ]; then
exit 255;
fi
choose
}
choose (){
dialog --title "Pick a choice" \
       --menu "Choose one" 12 35 5 \
       1 "Installation" \
       2 "check status" \
       3 "exit" 2> choose.txt
Choose=$(cat choose.txt)
if [[ $Choose == 1 ]]; then
config
elif [[ $Choose == 2 ]]; then
status_check
else 
exit 255
fi
}
status_check (){
systemctl status ospfd | grep Active | cut -d " " -f 5 > status.txt
systemctl status iptables | grep Active | cut -d " " -f 5 >> status.txt
systemctl status dhcpd | grep Active | cut -d " " -f 5 >> status.txt
systemctl status nsd | grep Active | cut -d " " -f 5 >> status.txt
systemctl status unbound | grep Active | cut -d " " -f 5 >> status.txt
systemctl status hostapd | grep Active | cut -d " " -f 5 >> status.txt
systemctl status network | grep Active | cut -d " " -f 5 >> status.txt
OSPF=$(cat status.txt | head -1)
IPTABLES=$(cat status.txt | head -2 | tail -1)
DHCP=$(cat status.txt | head -3 | tail -1)
NSD=$(cat status.txt | head -4 | tail -1)
UNBOUND=$(cat status.txt | head -5 | tail -1)
POSTAPD=$(cat status.txt | head -6 | tail -1)
NETWORK=$(cat status.txt | head -7 | tail -1)
if [[ $OSPF == "active"  ]]; then
 cat > status_check <<< "OSPF: [OK]"
else
 cat > status_check <<< "OSPF: [WRONG]"
fi
if [[ $IPTABLES == "active"  ]]; then
 cat >> status_check <<< "IPTABLES: [OK]"
else
 cat >> status_check <<< "IPTABLES: [WRONG]"
fi
if [[ $DHCP == "active"  ]]; then
 cat >> status_check <<< "DHCP: [OK]"
else
 cat >> status_check <<< "DHCP: [WRONG]"
fi
if [[ $NSD == "active"  ]]; then
 cat >> status_check <<< "NSD: [OK]"
else
 cat >> status_check <<< "NSD: [WRONG]"
fi
if [[ $UNBOUND == "active"  ]]; then
 cat >> status_check <<< "UNBOUND: [OK]"
else
 cat >> status_check <<< "UNBOUND: [WRONG]"
fi
if [[ $POSTAPD == "active"  ]]; then
 cat >> status_check <<< "WIRELESS: [OK]"
else
 cat >> status_check <<< "WIRELESS: [WRONG]"
fi
if [[ $NETWORK == "active"  ]]; then
 cat >> status_check <<< "NETWORK: [OK]"
else
 cat >> status_check <<< "NETWORK: [WRONG]"
fi
dialog --title "status check" \
       --backtitle "check" \
       --clear \
       --msgbox "$(cat /root/status_check)" 15 50
result=$?
if [ $result -eq 0 ] ; then
choose;
elif [ $result -eq 255 ]; then
exit 255;
fi
}
choose2 (){
dialog --title "Pick a choice" \
       --menu "Choose one" 12 35 5 \
       1 "Router Full Installation" \
       2 "Mail Full Installation" \
       3 "Seperate Installation" \
       4 "exit" 2> choose2.txt
Choose2=$(cat choose2.txt)
if [[ $Choose2 == 1 ]]; then
source ./netdev_bootstrap.sh  
source ./ospf_configuration.sh 
source ./iptable_configuration.sh
source ./dhcp_configuration.sh
source ./dns_configuration.sh
source ./wireless_lan_configuration.sh
elif [[ $Choose2 == 2 ]]; then
source ./mail_main.sh
elif [[ $Choose2 == 3 ]]; then
seperate
else
exit 255
fi
}

seperate (){
dialog --backtitle "Installation list" \
       --checklist "List" 20 50 10 \
       1 "ospf setting" 1 \
       2 "iptables setting" 2 \
       3 "dhcp setting" 3 \
       4 "dns setting" 4 \
       5 "wireless setting" 5 2>select.txt
read -r -a Select <<< $(cat select.txt)
case ${Select[0]} in
"1")
source ./ospf_configuration.sh 
echo "one";;
"2")
source ./iptable_configuration.sh
 echo "two" ;;
"3")
source ./dhcp_configuration.sh
 echo "three";;
"4")
source ./dns_configuration.sh
 echo "four" ;;
"5")
source ./wireless_lan_configuration.sh
 echo "five";;
esac
case ${Select[1]} in
"1")
source ./ospf_configuration.sh
 echo "one";;
"2")
source ./iptable_configuration.sh
 echo "two" ;;
"3")    
source ./dhcp_configuration.sh
 echo "three";;
"4")
source ./dns_configuration.sh
 echo "four" ;;
"5")
source ./wireless_lan_configuration.sh
 echo "five";;
esac
case ${Select[2]} in
"1")
source ./ospf_configuration.sh
 echo "one";;
"2")
source ./iptable_configuration.sh
 echo "two" ;;
"3")    
source ./dhcp_configuration.sh
 echo "three";;
"4")
source ./dns_configuration.sh
 echo "four" ;;
"5")
source ./wireless_lan_configuration.sh
 echo "five";;
esac
case ${Select[3]} in
"1")
source ./ospf_configuration.sh
 echo "one";;
"2")
source ./iptable_configuration.sh
 echo "two" ;;
"3")    
source ./dhcp_configuration.sh
 echo "three";;
"4")
source ./dns_configuration.sh
 echo "four" ;;
"5")
source ./wireless_lan_configuration.sh
 echo "five";;
esac
case ${Select[4]} in
"1")
source ./ospf_configuration.sh
 echo "one";;
"2")
source ./iptable_configuration.sh
 echo "two" ;;
"3")    
source ./dhcp_configuration.sh
 echo "three";;
"4")
source ./dns_configuration.sh
 echo "four" ;;
"5")
source ./wireless_lan_configuration.sh
 echo "five";;
esac
}
config (){ 
dialog --title "Setting configuration" \
       --form "Please input the infomation of the following:" 18 50 12 \
        "Router Hostname:" 1  1 "s14rtr.as2016.learn" 1  20  25  0 \
        "Router IP address:" 2  1 "14" 2  20  25  0 \
        "DHCP range start:" 3  1 "50" 3  20  25  0 \
        "DHCP range end:" 4  1 "100" 4  20  25  0 \
        "wireless DHCP start:" 5  1 "150" 5  20  25  0 \
        "wireless DHCP end:" 6  1 "200" 6  20  25  0 \
        "wireless SSID:" 7  1 "a00978401" 7  20  25  0 \
        "Mail username:" 8  1 "root" 8  20  25  0 \
        "Mail password:" 9  1 "P@ssw01rd" 9  20  25  0 \
        "Mail Hostname:" 10  1 "mail.s14.as2016.learn" 10  20  25  0 \
        "Mail IP address:" 11  1 "1" 11  20  25  0 \
        "Mail MAC address:" 12  1 "08:00:27:5b:b6:6a" 12  20  25  0 2> conf.txt
Routername=$(head -1 conf.txt)
RouterIP=$(head -2 conf.txt | tail -1)
DHCPstart=$(head -3 conf.txt | tail -1)
DHCPend=$(head -4 conf.txt | tail -1)
WirelessDHCPstart=$(head -5 conf.txt | tail -1)
WirelessDHCPend=$(head -6 conf.txt | tail -1)
SSID=$(head -7 conf.txt | tail -1)
USERNAME=$(head -8 conf.txt | tail -1)
PASSWORD=$(head -9 conf.txt | tail -1)
Mailname=$(head -10 conf.txt | tail -1)
DOMname=$(echo $Mailname|cut -d "." -f 2,3,4)
MailIP=$(head -11 conf.txt | tail -1)
MAC=$(tail -1 conf.txt)

if [[ $Routername == ""  ]]; then
 cat > conf_check <<< "Router Hostname: $Routername [WRONG]"
else
 cat > conf_check <<< "Router Hostname: $Routername [OK]"
fi
if [[ $RouterIP -le 254 && $RouterIP -ge 1  ]]; then
 cat >> conf_check <<< "Router IP address: $RouterIP [OK]"
else
 cat >> conf_check <<< "Router IP address: $RouterIP [WRONG]"
fi
if [[ $DHCPstart -le 126 && $DHCPstart -ge 1 && $DHCPstart -le $DHCPend  ]]; then
 cat >> conf_check <<< "DHCP start IP: $DHCPstart [OK]"
else
 cat >> conf_check <<< "DHCP start IP: $DHCPstart [WRONG]"
fi
if [[ $DHCPend -le 126 && $DHCPend -ge 1 && $DHCPend -ge $DHCPstart ]]; then
 cat >> conf_check <<< "DHCP end IP: $DHCPend [OK]"
else
 cat >> conf_check <<< "DHCP end IP: $DHCPend [WRONG]"
fi
if [[ $WirelessDHCPstart -le 254 && $WirelessDHCPstart -ge 128 && $WirelessDHCPstart -le $WirelessDHCPend  ]]; then
 cat >> conf_check <<< "Wireless DHCP start IP: $WirelessDHCPstart [OK]"
else
 cat >> conf_check <<< "Wireless DHCP start IP: $WirelessDHCPstart [WRONG]"
fi
if [[ $WirelessDHCPend -le 254 && $WirelessDHCPend -ge 128 && $WirelessDHCPend -ge $WirelessDHCPstart ]]; then
 cat >> conf_check <<< "Wireless DHCP end IP: $WirelessDHCPend [OK]"
else
 cat >> conf_check <<< "Wireless DHCP end IP: $WirelessDHCPend [WRONG]"
fi
if [[ $SSID == ""  ]]; then
 cat >> conf_check <<< "SSID name: $SSID [WRONG]"
else
 cat >> conf_check <<< "SSID name: $SSID [OK]"
fi
if [[ $USERNAME == ""  ]]; then
 cat >> conf_check <<< "Router User name: $USERNAME [WRONG]"
else
 cat >> conf_check <<< "Router User name: $USERNAME [OK]"
fi
if [[ $PASSWORD == ""  ]]; then
 cat >> conf_check <<< "Router password: $PASSWORD [WRONG]"
else
 cat >> conf_check <<< "Router password: $PASSWORD [OK]"
fi
if [[ $Mailname == ""  ]]; then
 cat >> conf_check <<< "Mail Hostname: $Mailname [WRONG]"
else
 cat >> conf_check <<< "Mail Hostname: $Mailname [OK]"
fi
if [[ $MailIP -le 126 && $MailIP -ge 1  ]]; then
 cat >> conf_check <<< "Mail IP address: $MailIP [OK]"
else
 cat >> conf_check <<< "Mail IP address: $MailIP [WRONG]"
fi
if [[ $MAC == ""  ]]; then
 cat >> conf_check <<< "Mail MAC address: $MAC [WRONG]"
else
 cat >> conf_check <<< "Mail MAC address: $MAC [OK]"
fi
config_check
} 
config_check (){ 
dialog --title "configuration" \
       --backtitle "your configuration" \
       --clear \
       --yes-label "SAVE" \
       --no-label "CHANGE" \
       --yesno "$(cat /root/conf_check)" 18 50
result=$?
if [ $result -eq 1 ] ; then
config;
elif [ $result -eq 0 ]; then
choose2
else
exit 255
fi
}
yesno

















