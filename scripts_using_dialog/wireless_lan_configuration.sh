#!/bin/bash - 
#===============================================================================
#
#          FILE: wireless_lan_configuration.sh
# 
#         USAGE: ./wireless_lan_configuration.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 28/04/16 10:02
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

echo "************************************"
echo "***setting wireless configuration***"
echo "************************************"

yum -y install hostapd &> /dev/null

WIRELESS=$(ip a | grep wlp | cut -d ":" -f 2 | cut -d " " -f 2 )

cat > /etc/sysconfig/network-scripts/ifcfg-${WIRELESS} <<EOF
DEVICE=${WIRELESS}
BOOTPROTO=none
TYPE=Wireless
ONBOOT=yes
NM_CONTROLLED=no
IPADDR=10.16.${RouterIP}.254
PREFIX=25
ESSID=${SSID}
CHANNEL=11
MODE=Master
RATE=Auto
EOF
cat > /etc/hostapd/hostapd.conf <<EOF
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
driver=nl80211
interface=${WIRELESS}
hw_mode=g
channel=11
ssid=${SSID}
EOF

echo "network 10.16.${RouterIP}.128/25 area 0" >> /etc/quagga/ospfd.conf

systemctl restart network
systemctl start hostapd
systemctl enable hostapd

iptables -t filter -A INPUT -s 10.16.${RouterIP}.128/25 -j ACCEPT
iptables -t filter -A FORWARD -s 10.16.${RouterIP}.128/25 -j ACCEPT

service iptables save

cat >> /etc/dhcp/dhcpd.conf <<EOF
subnet 10.16.${RouterIP}.128 netmask 255.255.255.128 {
      option routers 10.16.${RouterIP}.254;
      option  domain-name-servers 10.16.${RouterIP}.126;
      range 10.16.${RouterIP}.${WirelessDHCPstart} 10.16.${RouterIP}.${WirelessDHCPend};
      }
EOF
systemctl restart hostapd
systemctl restart dhcpd

sed -i "/interface:10.16/a\ interface:10.16.${RouterIP}.254" /etc/unbound/unbound.conf

sed -i "/access-control: 10.16/a\ access-control: 10.16.${RouterIP}.128/25 allow" /etc/unbound/unbound.conf
sed -i "s/DNS1=142.232.221.253/DNS1=127.0.0.1/" /etc/sysconfig/network-scripts/ifcfg-eth0
systemctl restart nsd
systemctl restart unbound
systemctl restart network
echo "Wireless setting is done"
