#!/bin/bash - 
#===============================================================================
#
#          FILE: ospf_configuration.sh
# 
#         USAGE: ./ospf_configuration.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 22/04/16 09:02
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

echo "*******************************"
echo "***setting ospf configuration***"
echo "*******************************"
declare -i IP1


hostnamectl set-hostname "$Routername"
echo "net.ipv4.ip_forward=1" > /etc/sysctl.conf
sysctl -p /etc/sysctl.conf

cat > /etc/sysconfig/network-scripts/ifcfg-eth0 << EOF
TYPE=Ethernet
BOOTPROTO=none
DEFROUTE=yes
NAME=eth0
DEVICE=eth0
ONBOOT=yes
IPADDR=10.16.255.${RouterIP}
PREFIX=24
GATEWAY=10.16.255.254
DNS1=142.232.221.253
EOF

echo "setting interface eth1"
cat > /etc/sysconfig/network-scripts/ifcfg-eth1 << EOF
TYPE=Ethernet
BOOTPROTO=none
DEFROUTE=yes
NAME=eth1
DEVICE=eth1
ONBOOT=yes
IPADDR=10.16.${RouterIP}.126
PREFIX=25
EOF

systemctl restart network
yum -y install quagga &> /dev/null
chown quagga:quagga /etc/quagga


cat > /etc/quagga/zebra.conf <<EOF
hostname ${Routername}
password ${PASSWORD}
log file /var/log/quagga/zebra.log
EOF


cat > /etc/quagga/ospfd.conf <<EOF
hostname ${Routername}
password ${PASSWORD}
log file /var/log/quagga/ospfd.log
router ospf
network 10.16.255.0/24 area 0
network 10.16.${RouterIP}.0/25 area 0
EOF

systemctl start zebra
systemctl enable zebra
systemctl start ospfd
systemctl enable ospfd
systemctl restart network
echo "ospf setting is done!"


