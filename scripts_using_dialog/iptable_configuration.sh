#!/bin/bash - 
#===============================================================================
#
#          FILE: iptable_configuration.sh
# 
#         USAGE: ./iptable_configuration.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 22/04/16 09:36
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

echo "************************************"
echo "***setting iptables configuration***"
echo "************************************"
yum -y install iptables iptables-services iptables-utils &> /dev/null
systemctl enable iptables
systemctl start iptables

iptables -F -t filter
iptables -F -t nat

iptables -X

iptables -t filter -P INPUT DROP
iptables -t filter -P FORWARD DROP

#All traffic should be allowed out of your router.
iptables -t filter -P OUTPUT ACCEPT

iptables -t filter -A INPUT -i lo -j ACCEPT

#SSH traffic should be allowed into your router VM and your mail VM.
iptables -t filter -A INPUT -p tcp --dport ssh  -j ACCEPT
iptables -t filter -A FORWARD -p tcp  --dport ssh    -j ACCEPT

#ICMP traffic shoul be allowed into your router.
iptables -t filter -A INPUT  -p icmp -j ACCEPT

#DNS traffic (both TCP and UDP) should be allowed into your router VM.
iptables -t filter -A INPUT  -p tcp --dport 53 -j ACCEPT
iptables -t filter -A INPUT  -p udp --dport 53 -j ACCEPT

#OSPF traffic should be allowed into your router VM.
iptables -t filter -A INPUT -d 224.0.0.5 -p ospf -j ACCEPT
iptables -t filter -A INPUT -d 224.0.0.6 -p ospf -j ACCEPT

#Establised connections should be allowed into your router.
iptables -t filter -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

#All established connections are allow back into your student wired network.
iptables -t filter -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT


#All traffic is allow to leave from your student wired network towards the class network and beyond.
iptables -t filter -A INPUT -s 10.16.14.0/25 -j ACCEPT
iptables -t filter -A FORWARD -s 10.16.14.0/25 -j ACCEPT
iptables -t filter -A FORWARD -p icmp -j ACCEPT



service iptables save
echo "setting iptables is done"


