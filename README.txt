Installation preparing

Three Virtual Mechines : 
1. Basic 
 (1).Memory : 768MB
 (2).HDD : 8GB Dynamically allocated
2. ASProuter
 (1). Two Network Interface
	a.Attached to Bridged Adapter : VLAN2016
	b.Attached to Internal Network : (create a name)
 (2). Adapter Type :Paravirtualized Network
3. Mail Server
 (1). Attached to Internal Network : (same name as ASProuter's Internal Network)
 (2). Adapter Type :Paravirtualized Network
 (3). Write down MAC address , it will be used later
4.Plug on USB for wireless
5.Install CentOS 7 Minimal

During Installation CentOS

1.The keyboard and language set to English Canada.
2.Vancouver Canada/Pacific Time Zone
3.Hard disk" is configured using "Automatic partitioning"
4.IP address configured staticly and auto-connect to network
5.Write down root user password, it will be used later

After Installation

On ASProuter
1.Login as root user
2.Make sure the USB for wireless adapter is connected
3.Download all files under folder "scripts_using_dialog"
4.Before run scripts, type "ip address" to make sure you have :
 (1).Two physical interfaces name : eth0 and eth1
 (2).One wireless interface 
5.Type "bash main.sh" to start 
6.After finish Router Installation, re-type "bash main.sh" to continue Mail Server Instrallation(You can also do this on Mail Server)

On Mail Server
If Mail Server is already install from ASProuter, you don't need to do anything.
Else, download file "mail_configuration.sh" under folder "scripts_using_dialog" and run it.


