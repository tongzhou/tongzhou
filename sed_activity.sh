#!/bin/bash - 
#===============================================================================
#
#          FILE: sed_activity.sh
# 
#         USAGE: ./sed_activity.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 16/05/16 08:35
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

sed -i s/'[^^]#.*'/''/g ./dhcpd.conf
cat ./dhcpd.conf
sed -i s/'^[[:space:]]*option[[:space:]]*routers[[:space:]]*[0-9]\{1,3\}.[0-9]\{1,3\}.[0-9]\{1,3\}.[0-9]\{1,3\}'/'option routers 10.10.0.1'/g ./dhcpd.conf
cat ./dhcpd.conf
range=$(cat ./dhcpd.conf | grep -E 'range[[:space:]]*[0-9]' | sed -e s/'range[[:space:]]*[0-9]\{1,3\}.[0-9]\{1,3\}.[0-9]\{1,3\}.'/''/| sed -e s/'[[:space:]]*[1-9]\{1,3\}.[1-9]\{1,3\}.[1-9]\{1,3\}.'/'-'/ | sed -e s/';.*'/''/) 
echo "The DHCP range is $range"
echo "Please enter a new DHCP range"
checkstart (){ 
read -p "DHCP start IP:" \
STARTIP
if [[ $STARTIP -ge 1 && $STARTIP -le 254 ]]; then 
checkend
else
echo "invalid range!!"
checkstart
fi
}
checkend (){
read -p "DHCP end IP:" \
ENDIP
if [[ $ENDIP -ge $STARTIP && $ENDIP -le 255 ]]; then
replaceDHCP
else
echo "invalid range!!"
checkend
fi
}
replaceDHCP (){
remain1=$(cat ./dhcpd.conf | grep -Eo 'range[[:space:]]*[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.')
sed -i s/'range[[:space:]]*[0-9]\{1,3\}.[0-9]\{1,3\}.[0-9]\{1,3\}.[0-9]\{1,3\}'/"${remain1}${STARTIP}"/ ./dhcpd.conf
remain2=$(cat ./dhcpd.conf | grep -Eo 'range[[:space:]]*[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}[[:space:]]*[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.')
sed -i s/'range[[:space:]]*[0-9]\{1,3\}.[0-9]\{1,3\}.[0-9]\{1,3\}.[0-9]\{1,3\}[[:space:]]*[0-9]\{1,3\}.[0-9]\{1,3\}.[0-9]\{1,3\}.[0-9]\{1,3\}'/"${remain2}${ENDIP}"/ ./dhcpd.conf
}
checkstart
cat ./dhcpd.conf
