#!/bin/bash - 
#===============================================================================
#
#          FILE: echo_main.sh
# 
#         USAGE: ./echo_main.sh 
# 
#   DESCRIPTION: enter script name and source it
# 
#       OPTIONS: ---
#  REQUIREMENTS: whereis_ck_arg.sh
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Tongzhou liu 
#  ORGANIZATION: 
#       CREATED: 15/04/16 13:14
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error



if [[ $# == 0 ]]; then
  echo "Please write at least one file name"
  exit 1
fi

source $1



