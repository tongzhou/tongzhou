#!/bin/bash - 
#===============================================================================
#
#          FILE: dhcp_configuration.sh
# 
#         USAGE: ./dhcp_configuration.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 19/04/16 15:24
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
declare -i IP1
declare -i IP2
declare -i START
declare -i END
declare -i ANSWER

yum -y install dhcp &> /dev/null
echo "********************************"
echo "***setting dhcp configuration***"
echo "********************************"

IP1=$(grep IPADDR /etc/sysconfig/network-scripts/ifcfg-eth0 | cut -d "." -f 4)
if [[ ${IP1} == "" ]]; then
 echo "you are not setting ospf"
 source ./main.sh
else
 echo "your router ID is detected"
fi
echo "Please enter DHCP range:"
range (){ 
startIP
endIP
}
rangecheck (){ if [[ $START -ge $END ]] ; then
 echo "invalid range"
 range
 rangecheck
else
 echo "your DHCP range will be ${START}-${END}"
fi
}
endIP (){ read -p "enter want IP you want to end?(1-125)" \
 END
 endcheck
}
startIP (){ read -p "enter want IP you want to start?(1-125)" \
 START
 startcheck
}
fixIPenter (){ read -p "enter want IP you want to specify?(1-125)" \
 IP2
 fixIPentercheck
}
startcheck (){ if [[ $START -ge 126 ]]; then
 echo -e "invalid IP\nTry again"
 startIP
fi
}
endcheck (){ if [[ $END -ge 126 ]]; then
 echo -e "invalid IP\nTry again"
 endIP
fi
}
fixIPentercheck (){ if [[ $IP2 -ge 126 ]]; then
 echo -e "invalid IP\nTry again"
 fixIPenter
fi
}
fixIP(){ read -p "Specify a fix IP address?(N or Y)" \
CHECK1
}
fixIPcheck (){ if [[ $CHECK1 = "n" || $CHECK1 = "N" ]]; then
 return 0
elif [[ $CHECK1 == y || $CHECK1 == Y ]]; then
 fixIPenter
 fixIPentercheck
 declare MAC
 read -p "enter the MAC address you want to specify?(XX:XX:XX:XX:XX:XX)" \
 MAC
 sed -i "/range 10.16/a\ host mail{\noption  domain-name-servers 10.16.${IP1}.126;\nhardware ethernet ${MAC};\nfixed-address 10.16.${IP1}.${IP2};\n}" /etc/dhcp/dhcpd.conf
 echo "MAC address is specified"
 else
 echo "invalid letter"
 fixIP
 fixIPcheck
fi
}
range
rangecheck
cat > /etc/dhcp/dhcpd.conf <<EOF
subnet 10.16.${IP1}.0 netmask 255.255.255.128 {
      option routers 10.16.${IP1}.126;
      option  domain-name-servers 10.16.${RouterIP}.126; 
      range 10.16.14.${START} 10.16.14.${END};
}
EOF
fixIP
fixIPcheck
cat /etc/dhcp/dhcpd.conf
sleep 2
systemctl start dhcpd
systemctl enable dhcpd
systemctl restart dhcpd
echo "DHCP setting is done"


