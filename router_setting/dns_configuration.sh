#!/bin/bash - 
#===============================================================================
#
#          FILE: dns_configuration.sh
# 
#         USAGE: ./dns_configuration.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 20/04/16 10:53
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

yum -y install nsd unbound &> /dev/null

echo "*******************************"
echo "***setting dns configuration***"
echo "*******************************"

IP1=$(grep IPADDR /etc/sysconfig/network-scripts/ifcfg-eth0 | cut -d "." -f 4)
if [[ ${IP1} == "" ]]; then
 echo "you are not setting ospf"
 source ./main.sh
else
 echo "your router ID is detected"
fi
IP2=$(grep fixed-address /etc/dhcp/dhcpd.conf | cut -d "." -f 4 | cut -d ";" -f 1)
if [[ ${IP2} == "" ]]; then
 echo "you are not setting dhcp"
 source ./maim.sh
else
 echo "your mailbox ID is detected"
fi

cat > /etc/nsd/nsd.conf <<EOF
server:
      ip-address: 10.16.255.$IP1
      do-ip6: no
      include: "/etc/nsd/server.d/*.conf"
      include: "/etc/nsd/conf.d/*.conf"
                          
remote-control:
      control-enable: yes
      zone:
      name: "s${IP1}.as2016.learn"
      zonefile: "s${IP1}.as2016.learn.zone"
      zone:
      name: "${IP1}.16.10.in-addr.arpa"
      zonefile: "${IP1}.16.10.in-addr.arpa.zone" 
EOF

cat > /etc/nsd/s${IP1}.as2016.learn.zone <<EOF
;zone file for s${IP1}.as2016.learn
\$TTL 10s                                ; 10 secs default TTL for zone
s${IP1}.as2016.learn.       IN SOA      s${IP1}rtr.as2016.learn. tongzhol0521.gmail.com.(
                        2016042001      ; serial number of Zone Record
                        1200s           ; refresh time
                        180s            ; update retry time on failure
                        1d              ; expiration time
                        3600            ; cache time to live
                        )
;Name servers for this domain
s${IP1}.as2016.learn.       IN      NS      s${IP1}rtr.as2016.learn.
                                    MX   10 mail.s${IP1}.as2016.learn.
;addresses of hosts
mail.s${IP1}.as2016.learn.  IN      A       10.16.${IP1}.${IP2}

EOF
    
cat > /etc/nsd/${IP1}.16.10.in-addr.arpa.zone <<EOF
;zone file for 10.16.${IP1}.0 / s${IP1}.as2016.learn reverse lookup
\$TTL 10s                                ;10 secs default TTL for zone
${IP1}.16.10.in-addr.arpa.   IN SOA         s${IP1}rtr.as2016.learn. tongzhol0521.gmail.com.(
                        2016042001      ; serial number of Zone Record
                        1200s           ; refresh time
                        180s            ; retry time on failure
                        1d              ; expiration time
                        3600            ; cache time to live
                        )
;Name servers for this domain
${IP1}.16.10.in-addr.arpa.          IN      NS      s${IP1}rtr.as2016.learn.
;IP to Hostname Pointers
${IP2}.${IP1}.16.10.in-addr.arpa.        IN      PTR     mail.s${IP1}.as2016.learn.
EOF

systemctl enable nsd
systemctl restart nsd

systemctl enable unbound
systemctl start unbound
wget -S -N https://www.internic.net/domain/named.cache -O /etc/unbound/root.hints &> /dev/null
cat > /etc/unbound/unbound.conf << EOF
	server:
        interface:10.16.${IP1}.126
        interface:127.0.0.1
        port: 53
        do-ip4: yes
        do-ip6: no
         access-control: 0.0.0.0/0 refuse
         access-control: 127.0.0.0/8 allow
         access-control: ::0/0 refuse
         access-control: 10.16.${IP1}.0/25 allow
        chroot: ""
        username: "unbound"
        directory: "/etc/unbound"
        root-hints: "/etc/unbound/root.hints"
        pidfile: "/var/run/unbound/unbound.pid"
        private-domain:"ne.learn"
        private-domain:"htpbcit.ca"
        local-zone: "10.in-addr.arpa." nodefault
        local-zone: "16.172.in-addr.arpa." nodefault
        local-zone: "168.192.in-addr.arpa." nodefault
        prefetch: yes
        module-config: "iterator"
remote-control:
        control-enable: yes
        server-key-file: "/etc/unbound/unbound_server.key"
        server-cert-file: "/etc/unbound/unbound_server.pem"
        control-key-file: "/etc/unbound/unbound_control.key"
        control-cert-file: "/etc/unbound/unbound_control.pem"
include: /etc/unbound/conf.d/*.conf
stub-zone:
        name: "learn"
        stub-addr: 142.232.221.253
stub-zone:
        name: "htpbcit.ca"
        stub-addr: 142.232.221.253
stub-zone:
        name: "${IP1}.16.10.in-addr.arpa"
        stub-addr: 10.16.255.${IP1}
stub-zone:
        name: "255.16.10.in-addr.arpa"
        stub-addr: 142.232.221.253
stub-zone:
        name: "s${IP1}.as2016.learn"
        stub-addr: 10.16.255.${IP1}
forward-zone:
        name:"bcit.ca"
        forward-addr:142.232.221.253
EOF

sed -i "s/DNS1=142.232.221.253/DNS1=127.0.0.1/" /etc/sysconfig/network-scripts/ifcfg-eth0
systemctl restart unbound
systemctl restart network 
echo "DNS setting is done"

