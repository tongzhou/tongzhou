#!/bin/bash - 
#===============================================================================
#
#          FILE: ospf_configuration.sh
# 
#         USAGE: ./ospf_configuration.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 22/04/16 09:02
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

echo "*******************************"
echo "***setting ospf configuration***"
echo "*******************************"
declare -i IP1
read -p "please enter your Router IP number (1-253): " \
  IP1
routerIPcheck (){ if [[ $IP1 -ge 254 ]]; then
 echo -e "invalid IP\nTry again"
 routerIPcheck
else
 echo -e "your eth0 IP address will be 10.16.255.${IP1}\nyour eth1 IP address will be 10.16.${IP1}.126"
fi
}
routerIPcheck
hostnamectl set-hostname "s${IP1}rtr.as2016.learn"
echo "net.ipv4.ip_forward=1" > /etc/sysctl.conf
sysctl -p /etc/sysctl.conf

echo "setting interface eth0"
cat > /etc/sysconfig/network-scripts/ifcfg-eth0 << EOF
TYPE=Ethernet
BOOTPROTO=none
DEFROUTE=yes
NAME=eth0
DEVICE=eth0
ONBOOT=yes
IPADDR=10.16.255.${IP1}
PREFIX=24
GATEWAY=10.16.255.254
DNS1=142.232.221.253
EOF

echo "setting interface eth1"
cat > /etc/sysconfig/network-scripts/ifcfg-eth1 << EOF
TYPE=Ethernet
BOOTPROTO=none
DEFROUTE=yes
NAME=eth1
DEVICE=eth1
ONBOOT=yes
IPADDR=10.16.${IP1}.126
PREFIX=25
EOF

systemctl restart network
yum -y install quagga &> /dev/null
chown quagga:quagga /etc/quagga


cat > /etc/quagga/zebra.conf <<EOF
hostname s${IP1}rtr.as2016.learn
password P@ssw01rd
log file /var/log/quagga/zebra.log
EOF


cat > /etc/quagga/ospfd.conf <<EOF
hostname s${IP1}rtr.as2016.learn
password P@ssw01rd
log file /var/log/quagga/ospfd.log
router ospf
network 10.16.255.0/24 area 0
network 10.16.${IP1}.0/25 area 0
EOF


systemctl start zebra
systemctl enable zebra
systemctl start ospfd
systemctl enable ospfd
systemctl restart network
echo "ospf setting is done!"


