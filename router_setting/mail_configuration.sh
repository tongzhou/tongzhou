#!/bin/bash - 
#===============================================================================
#
#          FILE: mail_configuration.sh
# 
#         USAGE: ./mail_configuration.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 03/05/16 15:47
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

read -p "enter your hostname:(ex:mail.sxx.as2016.learn)" \
MAILNAME
hostnamectl set-hostname $MAILNAME
cat > /etc/sysconfig/network-scripts/ifcfg-eth0 <<EOF
TYPE=Ethernet
BOOTPROTO=dhcp
DEFROUTE=yes
NAME=eth0
DEVICE=eth0
ONBOOT=yes
EOF
systemctl restart network

source ./netdev_bootstrap.sh

yum -y install postfix ntp ntpdate ntp-doc telnet openssl &> /dev/null
yum -y install dovecot &> /dev/null

systemctl start postfix
systemctl enable postfix
systemctl start ntpd
systemctl enable ntpd
systemctl start dovecot
systemctl enable dovecot

timedatectl set-timezone America/Vancouver
openssl req -x509 -newkey rsa:2048 -sha256 -nodes -keyout /etc/pki/tls/private/$MAILNAME.key -out /etc/pki/tls/certs/$MAILNAME.crt -days 7305 -subj "/C=CA/ST=British Columbia/L=Vancouver/O=BCIT/OU=NASP18/CN=$MAILNAME"


cat > /etc/postfix/main.cf << EOF
queue_directory = /var/spool/postfix
command_directory = /usr/sbin
daemon_directory = /usr/libexec/postfix
data_directory = /var/lib/postfix
mail_owner = postfix
inet_interfaces = all
inet_protocols = all
mydestination = \$myhostname, localhost.\$mydomain, localhost, \$mydomain
unknown_local_recipient_reject_code = 550
alias_maps = hash:/etc/aliases
alias_database = hash:/etc/aliases
home_mailbox = Maildir/
debug_peer_level = 2
debugger_command =
         PATH=/bin:/usr/bin:/usr/local/bin:/usr/X11R6/bin
         ddd \$daemon_directory/\$process_name \$process_id & sleep 5
sendmail_path = /usr/sbin/sendmail.postfix
newaliases_path = /usr/bin/newaliases.postfix
mailq_path = /usr/bin/mailq.postfix
setgid_group = postdrop
html_directory = no
manpage_directory = /usr/share/man
sample_directory = /usr/share/doc/postfix-2.10.1/samples
readme_directory = /usr/share/doc/postfix-2.10.1/README_FILES
smtpd_use_tls = yes
smtpd_tls_key_file  = /etc/pki/tls/private/$MAILNAME.key
smtpd_tls_cert_file = /etc/pki/tls/certs/$MAILNAME.crt
smtpd_tls_loglevel = 3
smtpd_tls_received_header = yes
smtpd_tls_session_cache_timeout = 3600s
tls_random_source = dev:/dev/urandom
EOF
cat > /etc/postfix/master.cf << EOF
smtp      inet  n       -       n       -       -       smtpd
smtps     inet  n       -       n       -       -       smtpd
  -o smtpd_tls_wrappermode=yes
  -o smtpd_sasl_auth_enable=yes
pickup    unix  n       -       n       60      1       pickup
cleanup   unix  n       -       n       -       0       cleanup
qmgr      unix  n       -       n       300     1       qmgr
tlsmgr    unix  -       -       n       1000?   1       tlsmgr
rewrite   unix  -       -       n       -       -       trivial-rewrite
bounce    unix  -       -       n       -       0       bounce
defer     unix  -       -       n       -       0       bounce
trace     unix  -       -       n       -       0       bounce
verify    unix  -       -       n       -       1       verify
flush     unix  n       -       n       1000?   0       flush
proxymap  unix  -       -       n       -       -       proxymap
proxywrite unix -       -       n       -       1       proxymap
smtp      unix  -       -       n       -       -       smtp
relay     unix  -       -       n       -       -       smtp
showq     unix  n       -       n       -       -       showq
error     unix  -       -       n       -       -       error
retry     unix  -       -       n       -       -       error
discard   unix  -       -       n       -       -       discard
local     unix  -       n       n       -       -       local
virtual   unix  -       n       n       -       -       virtual
lmtp      unix  -       -       n       -       -       lmtp
anvil     unix  -       -       n       -       1       anvil
scache    unix  -       -       n       -       1       scache
EOF

cat > /etc/dovecot/dovecot.conf << EOF
protocols = "imap lmtp pop3"
dict {
}
!include conf.d/*.conf
!include_try local.conf
ssl = yes
ssl_cert = </etc/pki/tls/certs/$MAILNAME.crt
ssl_key = </etc/pki/tls/private/$MAILNAME.key
EOF
cat > /etc/dovecot/conf.d/10-mail.conf  << EOF
mail_location = maildir:~/Maildir
namespace inbox {
  inbox = yes
}
mbox_write_locks = fcntl
EOF
cat > /etc/dovecot/conf.d/10-auth.conf  << EOF
disable_plaintext_auth = no
auth_mechanisms = plain
!include auth-system.conf.ext
EOF
systemctl restart dovecot.service

echo "mail server setting is done"
