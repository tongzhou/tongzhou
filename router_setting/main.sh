#!/bin/bash - 
#===============================================================================
#
#          FILE: mail.sh
# 
#         USAGE: ./mail.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 28/04/16 19:10
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

declare -i NUM


setnumber (){ 
echo -e "Please enter what you want to set\n1.basic setting\n2.ospf setting\n3.iptable setting\n4.dhcp setting\n5.dns setting\n6.wireless setting\n7.all setting"
read -p "Please enter a number(1-7) or "0" to quit:" \
NUM
checknumber
}
checknumber (){ if [[ $NUM -le 7 && $NUM -ge 1 ]]; then
 echo "valid number"
 installselect
elif [[ $NUM == 0 ]]; then
 echo "quit"
 exit 1
else 
 echo "inlalid number"
 setnumber 
fi
}
checkelse (){
read -p "Do you want to setting someting else?(N or Y)" \
CHECK
 if [[ $CHECK = "y" || $CHECK = "Y" ]]; then
source ./main.sh
elif [[ $CHECK == n || $CHECK == N ]]; then
 exit 1
else
 echo "invalid letter"
 checkelse
fi
}
installselect (){ case $NUM in
"1")
 source ./netdev_bootstrap.sh 
 echo "done"
 checkelse;;
"2")
 source ./ospf_configuration.sh
 echo "done"
 checkelse;;
"3")
 source ./iptable_configuration.sh
 echo "done"
 checkelse;;
"4")
 source ./dhcp_configuration.sh
 echo "done"
 checkelse;;
"5")
 source ./dns_configuration.sh
 echo "done"
 checkelse;;
"6")
 source ./wireless_lan_configuration.sh
 echo "done"
 checkelse;;
"7")
 touch test 
 source ./netdev_bootstrap.sh 
 source ./ospf_configuration.sh
 source ./iptable_configuration.sh
 source ./dhcp_configuration.sh
 source ./dns_configuration.sh
 source ./wireless_lan_configuration.sh
 rm -f test 
 echo "done"
 exit 0;;
esac
}

setnumber


