#!/bin/bash - 
#===============================================================================
#
#          FILE: wireless_lan_configuration.sh
# 
#         USAGE: ./wireless_lan_configuration.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 28/04/16 10:02
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

echo "************************************"
echo "***setting wireless configuration***"
echo "************************************"
echo "In order to set wireless configuration, ospf,iptable,dhcp and dns need to be set frist."
wirelesscheck (){ read -p "Are you finish?(N or Y)" \
CHECK
wirelesscheckenter
}
wirelesscheckenter (){ if [[ $CHECK == n || $CHECK == N ]]; then
 source ./main.sh
elif [[ $CHECK == y || $CHECK == Y ]]; then
 return 0
else
 echo "invalid letter" 
 wirelesscheck
 fi
}

yum -y install hostapd &> /dev/null

read -p "Create a name for your wireless:" \
NAME
IP1=$(grep IPADDR /etc/sysconfig/network-scripts/ifcfg-eth0 | cut -d "." -f 4)
WIRELESS=$(ip a | grep wlp | cut -d ":" -f 2 | cut -d " " -f 2 )
systemctl start hostapd
systemctl enable hostapd
cat > /etc/sysconfig/network-scripts/ifcfg-${WIRELESS} <<EOF
DEVICE=${WIRELESS}
BOOTPROTO=none
TYPE=Wireless
ONBOOT=yes
NM_CONTROLLED=no
IPADDR=10.16.${IP1}.254
PREFIX=25
ESSID=$NAME
CHANNEL=11
MODE=Master
RATE=Auto
EOF
cat > /etc/hostapd/hostapd.conf <<EOF
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
driver=nl80211
interface=${WIRELESS}
hw_mode=g
channel=11
ssid=$NAME
EOF

cat > /etc/quagga/ospfd.conf <<EOF
hostname s${IP1}rtr.as2016.learn
password P@ssw01rd
log file /var/log/quagga/ospfd.log
router ospf
network 10.16.255.0/24 area 0
network 10.16.${IP1}.0/25 area 0
network 10.16.${IP1}.128/25 area 0
EOF

systemctl restart hostapd

iptables -t filter -A INPUT -s 10.16.${IP1}.128/25 -j ACCEPT
iptables -t filter -A FORWARD -s 10.16.${IP1}.128/25 -j ACCEPT

service iptables save

echo "Please enter DHCP range:"
wirelessrange (){ 
wirelessstartIP
wirelessendIP
}
wirelessrangecheck (){ if [[ $START -ge $END ]] ; then
 echo "invalid range"
 wirelessrange
 wirelessrangecheck
else
 echo "your DHCP range will be ${START}-${END}"
fi
}
wirelessendIP (){ read -p "enter want IP you want to end?(129-254)" \
 END
 wirelessendIPcheck
}
wirelessstartIP (){ read -p "enter want IP you want to start?(129-254)" \
 START
 wirelessstartIPcheck
}
wirelessstartIPcheck (){ if [[ $START -gt 254 || $START -lt 129 ]]; then
 echo -e "invalid IP\nTry again"
 wirelessstartIP
fi
}
wirelessendIPcheck (){ if [[ $END -ge 254 || $START -lt 129 ]]; then
 echo -e "invalid IP\nTry again"
 wirelessendIP
fi
}

wirelessrange
wirelessrangecheck

DHCPCHECK=$(grep 10.16.${IP1}.128 /etc/dhcp/dhcpd.conf)
if [[ $DHCPCHECK == "" ]]; then
cat >> /etc/dhcp/dhcpd.conf <<EOF
subnet 10.16.${IP1}.128 netmask 255.255.255.128 {
      option routers 10.16.${IP1}.254;
      option  domain-name-servers 10.16.${IP1}.126;
      range 10.16.${IP1}.${START} 10.16.${IP1}.${END};
      }
EOF
else
echo "You already setting wireless DHCP"
fi

systemctl restart dhcpd

sed -i "/interface:10.16/a\ interface:10.16.${IP1}.254" /etc/unbound/unbound.conf

sed -i "/access-control: 10.16/a\ access-control: 10.16.${IP1}.128/25 allow" /etc/unbound/unbound.conf



systemctl restart unbound
systemctl restart network


