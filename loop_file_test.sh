#!/bin/bash - 
#===============================================================================
#
#          FILE: loop_file_test.sh
# 
#         USAGE: ./loop_file_test.sh 
# 
#   DESCRIPTION: test some files exist or not by a loop
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: tongzhou liu, 
#  ORGANIZATION: 
#       CREATED: 18/04/16 21:19
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

declare -ir argument=$#

if [[ $argument == 0 ]]; then
  echo "Please write at least one argument"
  exit 1
fi

function testfile {

declare file_name=$1

if [[ -f $file_name ]]; then
  echo "The filename:$1 exist"
else
  echo "The filename:$1 can't be found"
fi
}
  

for (( i=1 ; i <= $argument ; i++ )); do
  testfile $1
  shift
done

