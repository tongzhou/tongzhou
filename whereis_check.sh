#!/bin/bash - 
#===============================================================================
#
#          FILE: whereis_check.sh
# 
#         USAGE: ./whereis_check.sh 
# 
#   DESCRIPTION: enter command, if the command exist, display the command exist, and show the path, if the command not exist, display command not exist
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Tongzhou liu 
#  ORGANIZATION: 
#       CREATED: 15/04/16 13:14
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

declare command_name
declare output


read -p "please enter a name of command: " \
  command_name

output=$( whereis $command_name | cut -d ":" -f 2 )


if [[ $output == "" ]] ; then
  echo "$command_name not exist"
  exit 127
else
  echo "$command_name exist"
  echo -e "the path of this command is \n$output"
  exit 0
fi


