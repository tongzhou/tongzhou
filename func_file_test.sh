#!/bin/bash - 
#===============================================================================
#
#          FILE: func_file.test.sh
# 
#         USAGE: ./func_file.test.sh 
# 
#   DESCRIPTION: test file exist or not, all testing logic and output in a function
# 
#       OPTIONS: ---
#  REQUIREMENTS: 
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: tongzhou liu
#  ORGANIZATION: BCIT
#       CREATED: 18/04/2016 13:31
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error


function testfile {

declare file_name=$1

if [[ -f $file_name ]]; then
  echo "The filename:$1 exist"
  return 0
else
  echo "The filename:$1 can't be found"
  return 1
fi
}

testfile $1
