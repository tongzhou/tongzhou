#!/bin/bash - 
#===============================================================================
#
#          FILE: basic_file_test.sh
# 
#         USAGE: ./basic_file_test.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: tongzhou liu 
#  ORGANIZATION: 
#       CREATED: 17/04/16 19:45
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

declare file_name
declare exit_code

if [[ $# == 0 ]]; then
  echo "Please write at least one argument"
  exit 1
fi

file_name=$1  

if [[ -f $file_name ]]; then
  echo "The filename:$file_name exist"
  exit 0
else
  echo "The filename:$file_name can't be found"
  exit 1
fi



