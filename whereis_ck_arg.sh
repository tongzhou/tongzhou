#!/bin/bash - 
#===============================================================================
#
#          FILE: whereis_ck_arg.sh
# 
#         USAGE: ./whereis_ck_arg.sh 
# 
#   DESCRIPTION: run the script with an argument, and check the argument is a command or not
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: tongzhou liu
#  ORGANIZATION: 
#       CREATED: 15/04/16 13:14
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

declare command_name
declare output
  

if [[ $# == 0 ]];then
read -p "please enter a name of command: " \
  command_name
else
command_name=$1
fi

output=$( whereis $command_name | cut -d ":" -f 2 )


if [[ $output == "" ]] ; then
  echo "$command_name not exist"
  exit 127
else
  echo "$command_name  exist"
  echo -e "the path of this command is :\n $output"
  exit 0
fi


